/*******************************************************************************
 * 
 * Descripción: Clase que representa un nodo en un árbol binario
 *
 * Comentario:  Esta clase está pensada como una herramienta didáctica para 
 *              el curso de Estructuras de datos del grado en Informática 
 *              impartido en la Escuela Superior de Informática de Ciudad Real 
 *              de la Universidad de Castilla-La Mancha (Spain)
 * 
 * Autores: Alfonso Niño, Camelia Muñoz-Caro, and Crescencio Bravo
 * 
 * Última modificación: 8/11/2013
 * 
 ******************************************************************************/


public class NodeBinTree<T> {
  private NodeBinTree<T> parent;
  private T el;
  private NodeBinTree<T> left;
  private NodeBinTree<T> right;
  
  // Constructor
  public NodeBinTree(T e){
    parent=null;
    left=null;
    right=null;
    el=e;
  }
  
  // Establece el padre
  public void setParent(NodeBinTree<T> p){
    parent=p;
  }
  
  // Añade hijo izquierdo al nodo
  public void addLeft(NodeBinTree<T> n){
    left=n;
  }
  
  // Añade hijo derecho al nodo
  public void addRight(NodeBinTree<T> n){
    right=n;
  }
  
  // Devuelve hijo izquierdo
  public NodeBinTree<T> getLeft(){
    return left;
  }
    
  // Devuelve hijo derecho
  public NodeBinTree<T> getRight(){
    return right;
  }
  
  // Devuelve el padre del nodo
  public NodeBinTree<T> getParent(){
    return parent;
  }
  
  // Devuelve el elemento contenido en el nodo
  public T getElement(){
    return el;
  }
  
  //Determina si un nodo es hoja o no (nodo externo)
    public boolean isLeaf(){
    return (left==null && right==null);
  }
  
  /* Sobrescritura del método equals.
   * MUY IMPORTANTE: El equals de la clase Object
   * de Java acepta un parámetro de tipo Object, no genérico. Si se pone
   * como equals (T n) no es el mismo método (no se ha sobrescrito, se ha 
   * sobrecargado) y los métodos de la clase LinkedList como contains no 
   * funcionan (pues usan el método equals con Object).
   */
   
  public boolean equals (Object n){
    // Obsérvese el molde que convierte el Object n a tipo NodeBinTree<T>
    return el.equals(((NodeBinTree<T>)n).getElement());
  }
}
