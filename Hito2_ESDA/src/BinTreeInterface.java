/*******************************************************************************
 * 
 * Descripción: Interfaz que define la especificación funcional de un árbol 
 *              binario genérico 
 *
 * Comentario:  Esta interfaz está pensada como una herramienta didáctica para 
 *              el curso de Estructuras de datos del grado en Informática 
 *              impartido en la Escuela Superior de Informática de Ciudad Real 
 *              de la Universidad de Castilla-La Mancha (Spain)
 * 
 * Autores: Alfonso Niño, Camelia Muñoz-Caro, and Crescencio Bravo
 * 
 * Última modificación: 8/11/2013
 * 
 ******************************************************************************/


import java.util.Iterator;

public interface BinTreeInterface<T> {
  
  // Añade el elemento elem a la lista de nodos
  public void addNode (T elem);  
  
  // Devuelve el tamaño del árbol
  public int size(); 
  
  // Devuelve el nodo raíz del árbol
  public NodeBinTree<T> getRoot();
  
  // Establece como nodo raíz el que contenga el elemnto e
  public void setRoot(T e);
  
  // Añade el elemento e2 como hijo izquierdo de e1 (si e1 ya existe en el 
  // árbol). Si e1 o e2 no están en el árbol dispara una excepción. Si e1 ya tiene 
  // hijo izquierdo dispara otra excepción
  public void addLeft (T e1, T e2) throws ElementNotInTreeException,
                                   LeftChildExistentException;

  // Añade el elemento e2 como hijo derecho de e1 (si e1 ya existe en el 
  // árbol). Si e1 o e2 no están en el árbol dispara una excepción. Si e1 ya tiene 
  // hijo derecho dispara otra excepción
  public void addRight (T e1, T e2) throws ElementNotInTreeException,
                                    RightChildExistentException;
  
  // Devuelve el nodo que contiene el elemento elem
  public NodeBinTree<T> getNode(T elem);
  
  // Devolviendo el padre del nodo v
  public NodeBinTree<T> getParent(T elem) throws ElementNotInTreeException;
  
  // Devolviendo un iterador a los nodos del árbol
  public Iterator<NodeBinTree<T>> getNodes();
  
  // Comprobando si el nodo que contiene el elemento e es un nodo interno
  public boolean isInternal(T e) throws ElementNotInTreeException;
  
  // Comprobando si el nodo que contiene el elemento e es una hoja
  public boolean isExternal(T e) throws ElementNotInTreeException;
  
  // Comprobando si si el nodo que contiene el elemento e es la raíz del árbol
  public boolean isRoot(T e) throws ElementNotInTreeException;

}
