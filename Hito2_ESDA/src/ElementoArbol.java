class ElementoArbol {
	
	Integer id;
	char contenido;
	
	protected ElementoArbol(int identificacion, char contenidoarbol){
		id=identificacion;
		contenido=contenidoarbol;
	}//fin constructor1
	
	protected ElementoArbol(int identificador){
		id=identificador;
	}//fin constructor2
	
	protected int getIdElemento(){
		return id;
	}//fin getIdElemento
	
	protected char getContenidoElemento(){
		return contenido;
	}//fin getContenidoElemento
	
	protected void setdElemento(int iden){
		id=iden;
	}// fin setdElemento
	
	protected void setContenidoElemento(char cont){
		contenido=cont;
	}//fin setContenidoElemento
	
	public boolean equals (Object n){
		return id.equals(((ElementoArbol)n).getIdElemento());
	}//fin equals
}
