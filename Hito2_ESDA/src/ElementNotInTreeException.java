/*******************************************************************************
 * 
 * Descripción: Definición de la excepción para el caso de un elemento que no 
 *              se encuentra en el árbol considerado
 *
 * Comentario:  Esta excepción está pensada como una herramienta didáctica para 
 *              el curso de Estructuras de datos del grado en Informática 
 *              impartido en la Escuela Superior de Informática de Ciudad Real 
 *              de la Universidad de Castilla-La Mancha (Spain)
 * 
 * Autores: Alfonso Niño, Camelia Muñoz-Caro, and Crescencio Bravo
 * 
 * Última modificación: 8/11/2013
 * 
 ******************************************************************************/


public class ElementNotInTreeException extends
             RuntimeException {
  public ElementNotInTreeException (String err){
    super(err);
  }
}
