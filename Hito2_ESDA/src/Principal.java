//Realizado por Pedro Mac�as Rodriguez, Jos� Manuel Moreno L�pez de Lerma y Sergio P�rez S�nchez 
import java.io.*;
import java.util.*;

public class Principal {
	
	public static void main (String[]args) throws FileNotFoundException{
		File archivoarb=new File("arbol3.txt");
		File archivocod=new File("codificado3.txt");
		BinTree <ElementoArbol>arbol=crear_arbol(archivoarb);
		String cod=archivo_codificado(archivocod);
		List <Character>texto=descodificar(arbol, cod);
		imprimir(texto);
	}//fin main
	
	private static BinTree <ElementoArbol> crear_arbol(File archivoarb) throws FileNotFoundException{
		//Variables
		int numero_nodos=0, nodo_raiz=0, num_pa=0, num_hi=0, numero=0;
		String lado, caracter;
		char letra;
		BinTree <ElementoArbol> arbol=null;
		ElementoArbol nodo_padre, nodo_hijo, elemento;
		Scanner datosarb=new Scanner(archivoarb);
		
		//Lectura arbol.txt
		numero_nodos=datosarb.nextInt();
		nodo_raiz=datosarb.nextInt();
		datosarb.nextLine();
		elemento=new ElementoArbol(nodo_raiz,Character.MIN_VALUE);
		arbol=new BinTree <ElementoArbol> (elemento);
		
		for (int rec=1; rec<=numero_nodos; rec++){
			numero=datosarb.nextInt();
			caracter=datosarb.next();
			if(caracter.equalsIgnoreCase("L")){
				letra=datosarb.nextLine().charAt(2);
			}//fin if
			else {//Por tanto caracter.equalsIgnoreCase("I")
				letra=Character.MIN_VALUE;
			}//fin else
			elemento=new ElementoArbol(numero,letra);
			arbol.addNode(elemento);				
		}//fin for

		for (int rec=0; rec<numero_nodos-1; rec++){
			num_pa=datosarb.nextInt();
			num_hi=datosarb.nextInt();
			lado=datosarb.next();
			
			nodo_padre=new ElementoArbol(num_pa);
			nodo_hijo=new ElementoArbol(num_hi);
			
			if(lado.equalsIgnoreCase("Left")){
				arbol.addLeft(nodo_padre, nodo_hijo);
			}//fin if
			else {//Por tanto equalsIgnoreCase("Right")
				arbol.addRight(nodo_padre, nodo_hijo);
			}//fin else
		}//fin for

		datosarb.close();
		return arbol;
	}//fin metodo crear_arbol
		
	private static String archivo_codificado(File archivocod)throws FileNotFoundException{
		//Variables
		String binarios;
		Scanner datoscod=new Scanner(archivocod);
		
		//Lectura de codificado.txt
		binarios=datoscod.next();
		datoscod.close();
		return binarios;
	}//fin metodo archivo_codificado
	
	private static List<Character> descodificar(BinTree <ElementoArbol>arbol, String binarios){

		//Variables definidas
		ElementoArbol elemento;
		List <Character>texto=new LinkedList<Character>();
		//Recorrido del Arbol
		elemento=arbol.getRoot().getElement();
		
		for(int m=0; m<binarios.length();m++){
			if(binarios.charAt(m)=='1'){
				elemento=arbol.getNode(elemento).getRight().getElement();
			}//fin if
			else {//Por tanto binarios.charAt(m)=='0'
				elemento=arbol.getNode(elemento).getLeft().getElement();
			}//fin else
			
			if(arbol.isExternal(elemento)){
				texto.add(elemento.getContenidoElemento());
				elemento=arbol.getRoot().getElement();
			}//fin if
		}//fin for
		return texto;
	}//fin metodo descodificar
	
	private static void imprimir (List <Character> texto){
		for(int cont=0; cont<texto.size(); cont++){
			System.out.print(texto.get(cont));
		}//fin for
	}//fin metodo imprimir	
}//fin clase principal