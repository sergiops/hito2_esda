/*******************************************************************************
 * 
 * Descripción: Definición de la excepción para el caso de un hijo derecho ya 
 *              existente
 *
 * Comentario:  Esta excepción está pensada como una herramienta didáctica para 
 *              el curso de Estructuras de datos del grado en Informática 
 *              impartido en la Escuela Superior de Informática de Ciudad Real 
 *              de la Universidad de Castilla-La Mancha (Spain)
 * 
 * Autores: Alfonso Niño, Camelia Muñoz-Caro, and Crescencio Bravo
 * 
 * Última modificación: 8/11/2013
 * 
 ******************************************************************************/


public class RightChildExistentException extends
             RuntimeException {
  public RightChildExistentException (String err){
    super(err);
  }
}
