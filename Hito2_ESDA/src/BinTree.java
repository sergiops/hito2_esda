/*******************************************************************************
 * 
 * Descripción: Clase que representa un árbol binario con elementos genéricos.
 *              La clase implementa la especificación funcional definida en la 
 *              interfaz BinTreeInterface
 *
 * Comentario:  Esta clase está pensada como una herramienta didáctica para 
 *              el curso de Estructuras de datos del grado en Informática 
 *              impartido en la Escuela Superior de Informática de Ciudad Real 
 *              de la Universidad de Castilla-La Mancha (Spain)
 * 
 * Autores: Alfonso Niño, Camelia Muñoz-Caro, and Crescencio Bravo
 * 
 * Última modificación: 8/11/2013
 * 
 ******************************************************************************/


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class BinTree<T> implements BinTreeInterface<T> {
  private NodeBinTree<T> root=null;   // Raíz
  private List<NodeBinTree<T>> nodes; // Lista de nodos
  private int size=0;                 // Tamaño del árbol
  
  // Constructor simple
  public BinTree(){
    nodes=new LinkedList();
  }

  // Constructor que crea la raíz
  public BinTree(T el){
    NodeBinTree <T> n=new NodeBinTree(el);
    root=n;
    nodes=new LinkedList();
    nodes.add(n);  // Añadiendo el nodo raíz a la lista de nodos
    size++;
  }
  
  // Añade el elemento elem a la lista de nodos
  public void addNode (T elem){
    NodeBinTree<T> n=new NodeBinTree(elem);
    if (!nodes.contains(n)){  // Si el nodo no existe lo añade
      nodes.add(n);
      size++;
    }
  }
  
  // Devuelve el tamaño del árbol
  public int size(){
    return size;  // Otra opción es devolver nodes.size() y no usar la variable 
                  // de ejemplar size
  }
 
  // Devuelve el nodo raíz del árbol
  public NodeBinTree<T> getRoot(){
    return root;
  }
  
  // Establece como nodo raíz el que contenga el elemnto e
  public void setRoot(T e){
    NodeBinTree<T> n=new NodeBinTree(e);
    n=nodes.get(nodes.indexOf(n));
    root=n;
  }
  
  // Añade el elemento e2 como hijo izquierdo de e1 (si e1 ya existe en el 
  // árbol). Si e1 o e2 no están en el árbol dispara una excepción. Si e1 ya tiene 
  // hijo izquierdo dispara otra excepción
  
  public void addLeft (T e1, T e2) throws ElementNotInTreeException,
                                    LeftChildExistentException{

    NodeBinTree<T> n1= new NodeBinTree<T> (e1);
    NodeBinTree<T> n2= new NodeBinTree<T> (e2);

    if (nodes.contains(n1)){
      n1=nodes.get(nodes.indexOf(n1));
      if (n1.getLeft() != null) throw new LeftChildExistentException(
                               "Left child already in node "+e1);
      if (!nodes.contains(n2)) throw new ElementNotInTreeException
                               ("Element "+e2+" is not in this tree");
        
      n2=nodes.get(nodes.indexOf(n2));
      n1.addLeft(n2);
      n2.setParent(n1);
      
    }
    else {
      throw new ElementNotInTreeException("Element "+e1+" is not in this tree");
    }
  }
  
  // Añade el elemento e2 como hijo derecho de e1 (si e1 ya existe en el 
  // árbol). Si e1 o e2 no están en el árbol dispara una excepción. Si e1 ya tiene 
  // hijo derecho dispara otra excepción
  
  public void addRight (T e1, T e2) throws ElementNotInTreeException,
                                    RightChildExistentException{
    
    NodeBinTree<T> n1= new NodeBinTree<T> (e1);
    NodeBinTree<T> n2= new NodeBinTree<T> (e2);

    if (nodes.contains(n1)){
      n1=nodes.get(nodes.indexOf(n1));
      if (n1.getRight() != null) throw new RightChildExistentException(
                               "Right child already in node "+e1);
      if (!nodes.contains(n2)) throw new ElementNotInTreeException
                               ("Element "+e2+" is not in this tree");
  
      n2=nodes.get(nodes.indexOf(n2));
      n1.addRight(n2);
      n2.setParent(n1);
      
    }
    else {
      throw new ElementNotInTreeException("Element "+e1+" is not in this tree");
    }
  }
  
  // Devuelve el nodo que contiene el elemento elem
  public NodeBinTree<T> getNode(T elem){
    NodeBinTree<T> n=new NodeBinTree(elem);
    if (nodes.contains(n)){
      n=nodes.get(nodes.indexOf(n));
    }
    else {
      n=null;
    }
    return n;
  }
  
  // Devolviendo el padre del nodo v
  public NodeBinTree<T> getParent(T e) throws ElementNotInTreeException{

    NodeBinTree<T> v=new NodeBinTree<T> (e);
    if (nodes.contains(v)){
      v=nodes.get(nodes.indexOf(v));
      return v.getParent();
    }
    else {
      throw new ElementNotInTreeException("Element "+e+" is not in this tree");
    }
  }
  
  // Devolviendo un iterador a los nodos del árbol
  public Iterator<NodeBinTree<T>> getNodes(){
     return nodes.iterator();
   }
  
  // Comprobando si el nodo que contiene el elemento e es un nodo interno
  public boolean isInternal(T e) throws ElementNotInTreeException {
    NodeBinTree<T> n;
    NodeBinTree<T> v=new NodeBinTree<T> (e);
    if (nodes.contains(v)){
      n=nodes.get(nodes.indexOf(v));
      return (n.getLeft() != null || n.getRight()!=null);
    }
    else {
      throw new ElementNotInTreeException("Element "+e+" is not in this tree");
    }
  }
  
  // Comprobando si el nodo que contiene el elemento e es una hoja
  public boolean isExternal(T e) throws ElementNotInTreeException {
    NodeBinTree<T> n;
    NodeBinTree<T> v=new NodeBinTree<T> (e);
    if (nodes.contains(v)){
      n=nodes.get(nodes.indexOf(v));
      return (n.getLeft() == null && n.getRight() == null);
    }
    else {
      throw new ElementNotInTreeException("Element "+e+" is not in this tree");
    }
  }
  
  // Comprobando si el nodo que contiene el elemento e es la raíz del árbol
  public boolean isRoot(T e) throws ElementNotInTreeException {
    NodeBinTree<T> n;
    NodeBinTree<T> v=new NodeBinTree<T> (e);
    if (nodes.contains(v)){
      n=nodes.get(nodes.indexOf(v));
      return (n == root);
    }
    else {
      throw new ElementNotInTreeException("Element "+e+" is not in this tree");
    }
  }
  
  // Metodo que devuelve una una cadena que contiene los nodos del árbol  
  public String toString()  {
    return stringTree (root);
  }

  // Metodo recursivo usado por el método toString()       
  private String stringTree (NodeBinTree w) {
    String s=w.getElement().toString();
    boolean l=w.getLeft()!=null;
    boolean r=w.getRight()!=null;    
    if (l || r ){
      s+="[";

      if (l)
        s+=""+stringTree(w.getLeft());
      else
        s+="null";
      if (r)
        s += ", "+stringTree(w.getRight());
      else
        s+=", null";

      s+="]";
    }
    return s;
  }
}

